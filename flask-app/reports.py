##############################################################/
#
#Copyright (c) 2023 Esteban Egea-Lopez http://girtel.upct.es/~eegea/
#
##############################################################/


import graphviz
import base64
import shap
import pickle
from io import BytesIO,StringIO
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches
from flask import render_template 
from bokeh.plotting import figure
from bokeh.resources import CDN
from bokeh.embed import components 
from sklearn.tree import  export_graphviz
from classifiers import  get_model_feature_importances, get_class_ratios, get_standard_sets, get_transformed_dataset
from sklearn.inspection import PartialDependenceDisplay
from sklearn.preprocessing import OneHotEncoder, StandardScaler, MinMaxScaler
from sklearn.linear_model import LogisticRegression, SGDClassifier
from sklearn.tree import DecisionTreeClassifier, export_graphviz
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier, StackingClassifier, GradientBoostingClassifier
from preprocessing import ListEncoder 
from sklearn.pipeline import Pipeline
from sklearn.metrics import roc_auc_score
from shap import save_html
import xgboost


def mpl_to_base64(fig):
    buf = BytesIO()
    fig.savefig(buf, format="png")
    # Embed the result in the html output.
    data = base64.b64encode(buf.getbuffer()).decode("ascii")
    buf.close()
    plt.close(fig)
    return data

class SHAPReport():
  def __init__(self,model, X, y, fnames, important_features, important_class, translate=False, translation=None):
      self.model=model
      self.X = X
      self.y= y
      self.fnames = fnames
      self.important_features=important_features
      self.important_class=important_class
      if isinstance(model,(DecisionTreeClassifier,RandomForestClassifier,xgboost.XGBClassifier )):
          self.explainer = shap.TreeExplainer(model,feature_names=fnames)
          self.shap_values = self.explainer(X)
          self.expected_value=self.explainer.expected_value
      elif isinstance(model,(LogisticRegression, SGDClassifier )):
          masker=shap.maskers.Independent(X,max_samples=100)
          self.explainer = shap.explainers.Linear(model,masker,feature_names=fnames)
          self.shap_values = self.explainer(X)
          self.expected_value=self.explainer.expected_value
      else: 
          masker=shap.maskers.Independent(X,max_samples=100)
          self.explainer = shap.explainers.Exact(model.predict_proba,masker,feature_names=fnames)
          self.shap_values = self.explainer(X, max_evals=5000000)
          self.expected_value=self.shap_values.base_values[important_class]
      #Check the number of dimensions of the output
      tuplelen=len(self.shap_values.shape)
      if tuplelen>2:
         self.dim_v=self.shap_values.shape[2]
      else:
         self.dim_v=1
         #Fix important_class to 0
         self.important_class=0 

      #print('sv',self.sv)
      self.sv=self.shap_values[...,self.important_class]
      if translate:
        translate_features(translation)
        print('translate report', self.shap_values.feature_names)
  def set_X_display(self,ct, use_one_hots=False):
      transformers=ct.transformers_
      print(ct.output_indices_)
      tl={}
      for (n,t,c) in transformers:
         print(n,t,c)
         if isinstance(t, ListEncoder):
           print('le',c)
           #le[n]=t
         elif n !='remainder':
           print(c)
           tl[n]=t
      
      y=self.shap_values.data.copy()
      for k,v in tl.items():
         y[:,ct.output_indices_[k]]=v.inverse_transform(self.shap_values.data[:,ct.output_indices_[k]]) 
      self.shap_values.data=y 
  def translate_features(self, t):
      print('shap val', self.shap_values.data)
      ylabels=self.shap_values.feature_names
      n=[]
      imf=[]
      fn=[]
      for s in ylabels:
        if s in t.keys():
           n.append(t[s])
        else:
           n.append(s)
      for s in self.important_features:
        if s in t.keys():
           imf.append(t[s])
        else:
           imf.append(s)
      for s in self.fnames:
        if s in t.keys():
           print(t[s])
           fn.append(t[s])
        else:
           fn.append(s)
      self.shap_values.feature_names=n 
      self.important_features=imf
      self.fnames=fn
      self.sv=self.shap_values[...,self.important_class]
      
  def get_summary(self):
      plt.figure()
      #XGBoost provides log-odds and only for the important class
      if isinstance(self.model,xgboost.XGBClassifier ):
         shap_values2 = self.explainer.shap_values(self.X)
         print(type(shap_values2))
         print(shap_values2.shape)
         shap.summary_plot(shap_values2, self.X, show=False, feature_names=self.fnames)
      else:
         if self.important_class==0:
            shap.summary_plot(self.shap_values[...,0].values, self.X, show=False, feature_names=self.fnames)
         else:
            shap.summary_plot([self.shap_values[...,int(np.abs(self.important_class-1))].values, self.shap_values[...,self.important_class].values], self.X, show=False, feature_names=self.fnames)
      return plt.gcf()
  def get_beeswarm(self, max_display=-1):
      plt.figure()
      if isinstance(self.model,xgboost.XGBClassifier ):
          if (max_display>0):
              shap.plots.beeswarm(self.shap_values, show=False, max_display=max_display)
          else:
              shap.plots.beeswarm(self.shap_values, show=False)
      else:
          if (max_display>0):
             shap.plots.beeswarm(self.shap_values[:,:,self.important_class], show=False, max_display=max_display)
          else:
             shap.plots.beeswarm(self.shap_values[:,:,self.important_class], show=False)
      beeswarm=plt.gcf()
      beeswarm.gca().yaxis.label.set_size(1)
      beeswarm.set_size_inches(20, 16)
      return beeswarm
  def get_dependence(self,maxPlots=-1):
      dependence_plots=[]
      if maxPlots>=0:
        last=maxPlots
      else:
        last=len(self.important_features)
      n=0
      print('getde', self.important_features)
      for idx, i in enumerate(self.important_features):
         if n>last:
             break
         else:
            n = n+1
         plt.figure()
         print(i)
         if isinstance(self.model,xgboost.XGBClassifier ):
            shap.plots.scatter(self.shap_values[:,i], show=False, color=self.shap_values[:,:])
         else:
            shap.plots.scatter(self.shap_values[:,i,self.important_class], show=False, color=self.shap_values[:,:,self.important_class])
         dependence_plots.append(plt.gcf())
      return dependence_plots
  def model_proba(self,x):
       return self.model.predict_proba(x)[:,1] 
  def get_bar_cohort(self, max_display=10):
      plt.figure()
      if isinstance(self.model,xgboost.XGBClassifier ):
         shap.plots.bar(self.shap_values[:,:].cohorts(2), max_display=max_display,  show=False)
      else:
         shap.plots.bar(self.shap_values[:,:,self.important_class].cohorts(2), max_display= max_display, show=False)
      bar_cohort=plt.gcf()
      bar_cohort.set_size_inches(20, 16)
      return bar_cohort

  def get_bar_cluster(self):
      clustering = shap.utils.hclust(self.X, self.y)
      plt.figure()
      if isinstance(self.model,xgboost.XGBClassifier ):
         shap.plots.bar(self.shap_values, clustering=clustering, show=False)
      else:
         shap.plots.bar(self.shap_values[:,:,self.important_class], clustering=clustering, show=False)
      bar_cluster=plt.gcf() 
      bar_cluster.set_size_inches(20, 16)
      return bar_cluster

  def get_waterfall_sample(self, sv,max_display=10):
         plt.figure()
         if isinstance(self.model,xgboost.XGBClassifier ):
            shap.plots.waterfall(sv, show=False, max_display=max_display)
         else:
            shap.plots.waterfall(sv, show=False, max_display=max_display)
         plt.gcf().set_size_inches(15,10)
         return plt.gcf()
  def get_waterfall(self, max_display=10, max_range=100):
      w=[]
      for i in range(len(self.sv)):
         if i>=max_range:
            break
         plt.figure()
         if isinstance(self.model,xgboost.XGBClassifier ):
            shap.plots.waterfall(self.shap_values[i], show=False, max_display=max_display)
         else:
            shap.plots.waterfall(self.sv[i], show=False, max_display=max_display)
         plt.gcf().set_size_inches(15,10)
         w.append(plt.gcf())
      return w
  def get_force(self):
      force=[]
      for i in range(len(self.sv)):
          if isinstance(self.model,xgboost.XGBClassifier ):
              p=shap.plots.force(self.shap_values.base_values[0],self.shap_values[i].values, show=False,  feature_names=self.fnames,figsize=(20,5), link="logit")
          else:
              p=shap.plots.force(self.sv.base_values[0],self.sv[i].values, show=False,  feature_names=self.fnames,figsize=(20,5))
          force.append(p.html())
      return force
  def get_force_all(self):
      print(self.sv.shape)
      print(self.shap_values.shape)
      if isinstance(self.model,xgboost.XGBClassifier ):
         p=shap.plots.force(self.shap_values.base_values[0],self.shap_values.values, show=False,  feature_names=self.fnames,figsize=(20,5), link="logit")
      else: 
         p=shap.plots.force(self.sv.base_values[0],self.sv[:].values, show=False,  feature_names=self.fnames,figsize=(20,5))
      return p.html()
  def get_decision_all(self, filter=None):
      plt.figure()
      print(self.y)
      pr=self.model.predict_proba(self.X)[:,1]
      print(pr) 
      roc=roc_auc_score(self.y,pr,average=None)
      print('roc 1',roc)
      T= self.X
      pred=self.model.predict(self.X)
      if filter is None:
          misclassified=self.y.index[self.y!=pred]
      else:
          misclassified=filter

      print('misclassified',misclassified)
      sh=self.explainer.shap_values(T)[1]
      print(type(sh))

      p=shap.decision_plot(self.expected_value[1], sh[misclassified], T[misclassified], feature_order='importance', show=False, feature_names=self.fnames)
      dec=plt.gcf() 
      dec.set_size_inches(20, 16)
      return dec

def get_data_for_shap_report(job, dataset, trans,ley_d, gbi, important_class=1,use_one_hots=True):
    best=job.best_global
    #gbi=best["winners"][0]
    #print('winners',best['winners']) 
    #print('gbi',gbi) 
    model=best['model'][gbi]
    #print('model',model)
    name=best['name'][gbi]
    target=job.target
    predictors=best['predictors'][gbi]
    rt, rtr, rts = get_class_ratios(target,dataset,important_class )
    with open(job.results_path+str(job.id)+"-log.pkl", 'rb') as f:
        r=pickle.load(f)
    k=list(r.keys())
    print('keys',k)
    print(predictors)
    X, y, ct  =  get_transformed_dataset(dataset,target,predictors, trans, scale=True, use_one_hots=use_one_hots)
    #Get pretty feature names 
    important_features=[]
    if hasattr(model,'feature_importances_'):
        fev, fnames=get_model_feature_importances(dataset, model , target, predictors,trans, use_one_hots=use_one_hots) 
        aux=[]
        for (n,v) in fev:
            aux.append(n)
        
        important_features=split_feat_names(aux, replace=True, legend=ley_d)
    #print(important_features)        
    fnames=split_feat_names(ct.get_feature_names_out(), replace=True, legend=ley_d)
    #PDP only for numeric features 
    feat=[]
    for (n,t,l) in ct.transformers_:
      print(n)
      if  not isinstance(t,(ListEncoder, OneHotEncoder)):
         feat.append(ct.output_indices_[n].start)
    
    return [model,X,y,fnames, important_features, important_class]


def create_shap_report(job, dataset, trans,ley_d, gbi, important_class=1,use_one_hots=True):
    best=job.best_global
    #gbi=best["winners"][0]
    #print('winners',best['winners']) 
    #print('gbi',gbi) 
    model=best['model'][gbi]
    #print('model',model)
    name=best['name'][gbi]
    target=job.target
    predictors=best['predictors'][gbi]
    rt, rtr, rts = get_class_ratios(target,dataset,important_class )
    with open(job.results_path+str(job.id)+"-log.pkl", 'rb') as f:
        r=pickle.load(f)
    k=list(r.keys())
    print('keys',k)
    print(predictors)
    X, y, ct  =  get_transformed_dataset(dataset,target,predictors, trans, scale=True, use_one_hots=use_one_hots)
    #Get pretty feature names 
    important_features=[]
    if hasattr(model,'feature_importances_'):
        fev, fnames=get_model_feature_importances(dataset, model , target, predictors,trans, use_one_hots=use_one_hots) 
        aux=[]
        for (n,v) in fev:
            aux.append(n)
        
        important_features=split_feat_names(aux, replace=True, legend=ley_d)
    #print(important_features)        
    fnames=split_feat_names(ct.get_feature_names_out(), replace=True, legend=ley_d)
    #PDP only for numeric features 
    feat=[]
    for (n,t,l) in ct.transformers_:
      print(n)
      if  not isinstance(t,(ListEncoder, OneHotEncoder)):
         feat.append(ct.output_indices_[n].start)
    

    #Shap plots
    #try:
    shap_report=SHAPReport(model,X,y,fnames, important_features, important_class=important_class)
       
    #except TypeError:
    #   print('Model cannot be used with SHAP')
    #   shap_report=None
    
    return name, predictors, target, rt,  shap_report

def make_histogram_plot(title, hist, edges):
    p = figure(title=title, tools='', background_fill_color="#fafafa")
    p.quad(top=hist, bottom=0, left=edges[:-1], right=edges[1:],
           fill_color="navy", line_color="white", alpha=0.5)
    return p

def make_plot(x,y, title, xlabel,ylabel):
    print(x)
    #p = figure(title = title, sizing_mode="scale_both", width=400, height=400)
    p = figure(title = title, sizing_mode="fixed", width=600, height=400)
    p.xaxis.axis_label =xlabel 
    p.yaxis.axis_label = ylabel
    p.line(x,y,line_width=2)
    return p
def make_plot_cat(x,y, title,xlabel="",ylabel=""):
    p = figure(x_range=x, title = title, sizing_mode="fixed", width=600, height=400)
    p.xaxis.axis_label =xlabel 
    p.yaxis.axis_label = ylabel
    p.line(x=x,y=y, line_width=2)
    return p
def make_plot_tree(model,fnames):
   dot_data=export_graphviz(model,out_file=None,feature_names=fnames, class_names=["No", "Si"], proportion=True, filled=True) 
   graph=graphviz.Source(dot_data)
   #png=graph.pipe(format='svg')
   png=graph.pipe(format='png')
   data=base64.b64encode(png)
   return data.decode() 

def sel_feat_report(job, dataset, trans,  verbose=False):
    use_one_hots=job.use_one_hots
    best=job.best_global
    #If more than one winner, use the fewest predictors
    gbi=best["winners"][-1] 
    model=best['model'][gbi]
    name=best['name'][gbi]
    target=job.target
    predictors=best['predictors'][gbi]
    important_class = job.important_class
    rt, rtr, rts = get_class_ratios(target,dataset,important_class )
    if verbose:
      print('winners',best['winners']) 
      print('gbi',gbi) 
      print('model',model)
      print(job.results_path+str(job.id)+"-log.pkl") 
    with open(job.results_path+str(job.id)+"-log.pkl", 'rb') as f:
        r=pickle.load(f)
    k=list(r.keys())
    #Get all scores for best global model
    summa=[] 
    bi=r[k[gbi]]['bestind']
    bm=r[k[gbi]]['all'][bi]
    prf=bm["scores"]
    #print(prf)
    d={'n':name,'sc':prf, 'pre': predictors, 'mid': gbi }
    #print(d["sc"][0])
    summa.append(d)
    #Feature importances
    fe_list=[]
    fev=None
    bi=r[k[gbi]]['bestind']
    bm=r[k[gbi]]['all'][bi]
    if verbose:
       print('keys',k)
       print('k',k[gbi])
       print('bi',bi)
       print('best name', bm['name'])
       print('model', bm['model'])
       print('n_features_in_',bm['n_features_in_'])
       print('predictors', bm['cpred'])
       print('n_features_in_',bm['model'].n_features_in_)
    has_importances=False
    if hasattr(bm['model'],'feature_importances_'):
       fev, fnames=get_model_feature_importances(dataset, bm['model'] , job.target, bm['cpred'],trans, use_one_hots=use_one_hots ) 
       has_importances=True
       fe_list.append(fev)
   #Tree plots
    tplots=[]
    if name == "DecisionTree":
       tp=make_plot_tree(bm['model'],fnames)
       tplots.append(tp)
    for i in best["winners"]:
        if i!=gbi:
            bi=r[k[i]]['bestind']
            bm=r[k[i]]['all'][bi]
            prf=bm["scores"]
            n=best['name'][i]
            pre=best["predictors"][i]
            d={'n':n,'sc':prf,'pre': pre ,'mid': i}
            summa.append(d)
            if hasattr(bm['model'],'feature_importances_'):
              fev, fnames=get_model_feature_importances(dataset, bm['model'] , job.target, bm['cpred'],trans, use_one_hots=use_one_hots) 
              has_importances=True
              fe_list.append(fev)
            if n == "DecisionTree":
               tp=make_plot_tree(bm['model'],fnames)
               tplots.append(tp)
                    #if n is "DecisionTree":
    if verbose: 
        print(best["winners"])
        print(best["name"][gbi])
        print(best["predictors"][gbi])
        print(best["scores"][gbi])
        print(best["scores"])
        #print(np.arange(len(best['name'])))
        #for k in r.keys():
           #print(k)
        #print(r)
    x=np.arange(len(best['name'])).tolist()
    #Several plots
    label="F1"
    if job.metric==1:
      label="Recall"
    elif job.metric==2:
      label="Precision"

    label=label +" for class "+str(important_class)
    f=make_plot_cat(list(r.keys()),best["scores"],'Best models', xlabel="Predictors-Combinations",ylabel=label)
    script_bmodels, div_bmodels = components(f)
    
    return name, predictors, target, script_bmodels, div_bmodels, summa, fe_list, has_importances, tplots,rt 

def split_feat_names(names, replace=False, legend=None):
    sn=[]
    for i in names:
       print(i)
       x=i.split("__")
       if "lel" in x[-1]:
           sn.append(''.join(x))
       else:
         if replace and legend is not None:
           sb=x[-1].split('_')
           print(x[0])
           print(sb)
           if (len(sb)>1):
              print(legend)
              val=legend[x[0]]["legend"][int(float(sb[-1]))]
              print(val)
              sb[-1]=val
           sn.append(' '.join(sb))
         else:  
           sn.append(x[-1])
       #print(sn)
    return sn

