##############################################################/
#
#Copyright (c) 2023 Esteban Egea-Lopez http://girtel.upct.es/~eegea/
#
##############################################################/

import numpy as np
import pandas as pd
import os
import pickle
import time
import sys
#import matplotlib.pyplot as plt
from copy  import deepcopy
from itertools import combinations
from scipy.special import comb
from os import path
from preprocessing import  check_nan_in_columns, number_of_classes,   np_html, subs_ley, classes_to_dict, load_data,createColumnTransformer
from classifiers import  generate_classifiers,  evaluate_classifiers_transformer, get_model_feature_importances

def computeCombinations(predictors, step) -> int:
   total=0
   print('predictors',predictors,'step',step)
   for j in range(predictors,0,-step):
     k=j-step
     if k<0:
        break
     total=total +comb(j,k) 
   print('total',total)
   return total
def computeAllCombinations(predictors) -> int:
   total=0
   for j in range(2,predictors+1):
     total=total +comb(predictors,j) 
   return total
class searchToFile:
   def __init__(self, folderpath="."):
       self.folderpath=folderpath
   def __call__(self,results):
         j=results['number_predictors']
         ncomb=results['number_combinations']
         cdone=results['combinations_tested']
         time=results['time']
         savename=self.folderpath+"/pred"+str(j)+"-c"+str(ncomb)
         bestind=results['bestind']
         winners=results['winners']
         scores=results['all'][bestind]['scores']
         #models=results['names']
         bestmodels=results['all'][bestind]['model']
         cpred=results['all'][bestind]['cpred']
         np.savetxt(savename+"-scores.txt",(scores),delimiter="\t")
         with open(savename+"-models-scores.pkl", 'wb') as f:
              pickle.dump(results['all'], f, pickle.HIGHEST_PROTOCOL)
         with open(savename+"-cpred-scores.pkl", 'wb') as f:
              pickle.dump(cpred, f, pickle.HIGHEST_PROTOCOL)
         with open(savename+"-winners-scores.pkl", 'wb') as f:
              pickle.dump(winners, f, pickle.HIGHEST_PROTOCOL)
         with open(savename+"-best-model.pkl", 'wb') as f:
              pickle.dump(bestmodels, f, pickle.HIGHEST_PROTOCOL)





def search_features_selection(dataset, full_predictors, r, target, trans,number_of_classes, important_class, metric,classifiers_fn=None, verbose=False, progress_callback=None, toFolder=False, folderpath="", fullSearch=False,use_one_hots=True):   
   globalbest={'name':[],'predictors':[],'scores':[], 'winners':[],'model':[], 'gbi':-1}
   totalcomb=0
   log={}
   predictors=full_predictors
   print('Starting feature selection algorithm')
   print('Number_of_classes of the target variable :',number_of_classes, '. Class of interest (important_class):', important_class)
   if metric==0:
     sc_i=number_of_classes*2+important_class
     print('Evaluating f1')
   elif metric==1:
     print('Evaluating recall')
     sc_i=number_of_classes+important_class
   elif metric==2:
     print('Evaluating precision')
     sc_i=important_class
   if fullSearch:
      print('Full search')
      r=1
   #Start testing all predictors
   ncomb=len(full_predictors)
   #Test combinations
   for j in range(len(full_predictors),0,-r):
      start=time.time()
      if ncomb<=0:
        break
      #List with all the combinations of the current predictor list
      cpred=list(combinations(predictors,ncomb))
      n_predictors=len(predictors)
      print('Number of predictors',n_predictors, '. Combinations',ncomb, 'Number of combinations at iteration', len(cpred))
      cdone=len(cpred)
      totalcomb = totalcomb + cdone
     #Auxiliary variables for metrics
      recalls=[]
      bestind=[]
      bestmodelnames=[]
      bestmodels=[]
      bestpred=[]
      cts=[]
      scores=np.empty((0,9),dtype=np.float64)
      results={}
      lre={}
      for idx, i in enumerate(cpred):
         lre[idx]={}
         predictors=list(cpred[idx])
         ct, _=createColumnTransformer(predictors,trans, use_one_hots=use_one_hots)
         evaldataset=dataset 
         nans_features, has_nan=check_nan_in_columns(dataset[predictors], trans)
         if has_nan:
             evaldataset=dataset.copy()
             #Remove missing values in predictors
             for pred in predictors:
                sel=trans[trans['name']==pred]
                if not sel['type'].values==2 : #nan/missings are allowed  in tooth lists since it is assumed that no tooth was in the list
                    evaldataset.dropna(subset=pred,inplace=True)
         print("Predictors tested",predictors)
         if classifiers_fn is None:
            classifiers=generate_classifiers()
         else:  
           classifiers=classifiers_fn()
         #Do evaluation
         d, bestd =evaluate_classifiers_transformer(evaldataset, target, predictors,ct, number_of_classes, classifiers, important_class, metric, verbose=False)
         recalls.append(bestd['scores'][1][1])
         #print(np.concatenate(bestd['scores']).reshape((1,8)))
         scores=np.append(scores,np.concatenate(bestd['scores']).reshape((1,9)), axis=0)
         bm=bestd['model']
         lre[idx]['name']=bestd['name']
         lre[idx]['cpred']=predictors
         lre[idx]['model']=bm
         lre[idx]['n_features_in_']=bestd['model'].n_features_in_
         assert lre[idx]['n_features_in_']==bestd['model'].n_features_in_
         assert lre[idx]['n_features_in_']==bm.n_features_in_
         lre[idx]['name']=bestd['name']
         lre[idx]['scores']=np.concatenate(bestd['scores'],axis=0)
      
      bestind=np.argmax(scores[:,sc_i])
      if verbose:
         print('index for best model found',bestind)
         print('scores of best model', scores[bestind,:])
         print('score of evaluated metric', scores[bestind,sc_i])
      results['all']=lre
      results['winners'] = np.flatnonzero(scores[:,sc_i] == np.max(scores[:,sc_i]))
      results['bestind']=bestind
      results['number_predictors']=n_predictors
      results['number_combinations']=ncomb
      results['combinations_tested']=cdone
      end=time.time()
      results['time']=end-start
      
      logkey=str(n_predictors)+"-"+str(ncomb) 
      log[logkey]=results
      #Optionally save intermediate results to folder 
      if toFolder:
         savename=folderpath+str(j)+"-c"+str(ncomb)
         np.savetxt(savename+"-scores.txt",(scores),delimiter="\t")
         with open(savename+"-models-scores.pkl", 'wb') as f:
              pickle.dump(results['all'], f, pickle.HIGHEST_PROTOCOL)
         with open(savename+"-cpred-scores.pkl", 'wb') as f:
              pickle.dump(lre[bestind]['cpred'], f, pickle.HIGHEST_PROTOCOL)
         with open(savename+"-winners-scores.pkl", 'wb') as f:
              pickle.dump(results['winners'], f, pickle.HIGHEST_PROTOCOL)
         with open(savename+"-best-model.pkl", 'wb') as f:
              pickle.dump(lre[bestind]['model'], f, pickle.HIGHEST_PROTOCOL)
      #Call callbacks
      if progress_callback is not None:
         for ca in progress_callback:
            ca(results)  
      #Update global results
      globalbest['scores'].append(np.max(scores[:,sc_i]))
      globalbest['name'].append(lre[bestind]['name'])
      globalbest['predictors'].append(lre[bestind]['cpred'])
      globalbest['model'].append(lre[bestind]['model'])
  
      #Print info
      print('Equal winners', results['winners'])
      #print('SC-',j,'-',ncomb,' Global best',bestmodelnames[bestind], cpred[bestind], np.max(scores[:,sc_i]))   
      print('SC-',len(predictors),'-',ncomb,' Global best',lre[bestind]['name'], lre[bestind]['cpred'], lre[bestind]['scores'])   
      print('time', end-start, 's')
 
      #New set of predictors
      if fullSearch:
         predictors=full_predictors
         ncomb=j-1
      else:
         predictors=cpred[bestind]
         ncomb=j-r
      print('New predictors', predictors)



   print('Total combinations tested', totalcomb)
   print(globalbest['scores'])
   gbi=np.argmax(globalbest["scores"])
   print('Global best index',gbi)
   globalbest['gbi']=gbi
   print('Global best combination', gbi, globalbest["name"][gbi],globalbest["predictors"][gbi],globalbest["scores"][gbi])
   globalbest['winners'] = np.flatnonzero(globalbest["scores"] == globalbest["scores"][gbi])
   print('Equal global winners', globalbest['winners'])
   
   return globalbest, log
 
if __name__ == '__main__':
         sys.exit(main()) 
