##############################################################/
#
#Copyright (c) 2023 Esteban Egea-Lopez http://girtel.upct.es/~eegea/
#
##############################################################/


import re
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.rcsetup as rcsetup
import pickle
import graphviz
import xgboost
from sklearnex import patch_sklearn

patch_sklearn()
from sklearn.metrics import  confusion_matrix, precision_recall_fscore_support, precision_score, recall_score, roc_curve, precision_recall_curve

from sklearn.linear_model import LogisticRegression, SGDClassifier
from sklearn.tree import DecisionTreeClassifier, export_graphviz
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier, StackingClassifier, GradientBoostingClassifier
from sklearn.preprocessing import LabelEncoder, StandardScaler
from sklearn.preprocessing import OneHotEncoder
from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn.neighbors import KNeighborsClassifier
from sklearn.naive_bayes import GaussianNB, CategoricalNB, ComplementNB
from sklearn.impute import SimpleImputer
from sklearn.gaussian_process import GaussianProcessClassifier
from sklearn.gaussian_process.kernels import RBF
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis
from sklearn.neural_network import MLPClassifier
from sklearn.svm import SVC
from sklearn.compose import ColumnTransformer
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import cross_val_predict
from sklearn.calibration import CalibratedClassifierCV

from tempfile import mkdtemp
from sklearn.decomposition import PCA
from sklearn.inspection import permutation_importance
from preprocessing import  number_of_classes, check_nan_in_columns, createColumnTransformer
from sklearn.metrics import PrecisionRecallDisplay, plot_precision_recall_curve, roc_curve, auc, roc_auc_score
from dtreeviz.trees import *
from sklearn.feature_selection import RFECV, SequentialFeatureSelector, SelectFromModel
from sklearn.base import clone

#Any other suitable list of classifiers may be generated here
#Classifiers parameters may be tuned also
def generate_classifiers():
    #Try CategoricalNB. Requires particular codification of the input features, see user guide
    estimators = [('rf', RandomForestClassifier(n_estimators=100, random_state=42)),('gnb',GaussianNB())]
    clf = StackingClassifier(estimators=estimators, final_estimator=LogisticRegression())
    classifiers={'ComplementNB': ComplementNB(),'SGDClassifier': SGDClassifier(loss='modified_huber'),'GaussianNB': GaussianNB(), 'LogisticRegression': LogisticRegression(multi_class='auto'), 'DecisionTree': DecisionTreeClassifier(criterion='entropy'),  'RandomForest': RandomForestClassifier(),  'GaussianProcessClassifier': GaussianProcessClassifier(1.0*RBF(1.0)),'SVC Linear': SVC(kernel="linear", C=0.025, probability=True),'SVC':  SVC(gamma=2, C=1, probability=True),'AdaBoost': AdaBoostClassifier(),'MLP': MLPClassifier(alpha=1, max_iter=1000), 'Stacking': clf, 'XGBoost': xgboost.XGBClassifier()}
    return classifiers


def plot_pr_curve(pr, rec, th,label):
    plt.plot(rec,pr, label=label)
    plt.xlabel("Recall")
    plt.ylabel("Precision")
    plt.legend()

def plot_roc_curve(fpr,tpr, label=None):
    plt.plot(fpr,tpr, linewidth=2, label=label)
    plt.plot([0,1],[0,1],'k--')

def plot_pr_curves_classifiers(dataset, classifiers):
    train_set, test_set = train_test_split(dataset, test_size=0.2, random_state=42)
    y=train_set[:,0]
    print('y shape=',y.shape)
    td=train_set[:,1:]
    for k, v in classifiers.items():
        #cl=DecisionTreeClassifier(random_state=42)
        print(k)
        #p=cross_val_score(v,td,y,cv=3,scoring="accuracy")
        #print('p=',p)
        if (k=='GaussianNB' or k=='DecisionTree' or k=='RandomForest' or k=='GaussianProcessClassifier' or k=='MLP'):
            print ('proba', k)
            y_scores=cross_val_predict(v,td,y,cv=3,method="predict_proba")
            print(y_scores.shape)
            pre, re, th = precision_recall_curve(y,y_scores[:,1]) 
        else:
            print ('df',k)
            y_scores=cross_val_predict(v,td,y,cv=3,method="decision_function")
            pre, re, th = precision_recall_curve(y,y_scores) 
        #fpr, tpr, thresholds = roc_curve(y, y_scores)
        #plot_roc_curve(fpr,tpr,label=k)
        
        plot_pr_curve(pre,re,th, k)
        plt.show()
       

    


def evaluate_classifiers_transformer(dataset, target, predictors, ct, classes, classifiers, important=1, metric=0, verbose=True):
    #Create dict to return data
    d={}
    train_set, test_set = train_test_split(dataset, test_size=0.2, random_state=42)
    if verbose:
        print('Complete train_set:',train_set)
    y_train=train_set[[target]].values.ravel()
    xt=train_set[predictors].copy()
    if xt.isnull().values.any():
             print('Train set  column contains NaN')
             #print(xt.isnull())
    X_train=ct.fit_transform(xt)
    if verbose:
        print('y (train set)=',y_train)
        print('train_set X=',X_train)
        print('train_set X.shape=',X_train.shape)
    f_scores=[]
    recalls=[]
    precisions=[]
    #Evaluate all classifiers passed
    for k, v in classifiers.items():
        d[k]={}
        y_train_pred=cross_val_predict(v,X_train,y_train,cv=3)
        if verbose:
            print('classifier', k)
            print('y_train_pred.shape',y_train_pred.shape)
        cf=confusion_matrix(y_train,y_train_pred)
        d[k]['cf']=cf
        if verbose:
           print('confusion matrix:')
           print(cf)
        
        if classes>2:
            p, r, f, s=precision_recall_fscore_support(y_train, y_train_pred, average='weighted',zero_division=0)
            f_scores.append(f)
            if verbose:
                print('Multiclass p=',p,'r=',r,'f=',f,'s=',s)
        else:
            p, r, f, s=precision_recall_fscore_support(y_train, y_train_pred, zero_division=0)
            f_scores.append(f[important])
            recalls.append(r[important])
            precisions.append(p[important])
            if verbose:
                print('p=',p,'r=',r,'f=',f,'s=',s)
        d[k]['scores']=[p,r,f,s]
    if verbose:
        print('f_scores:',f_scores)
        print('precisions:',precisions)
        print('recalls:',recalls)
    #F1 score
    if metric==0:
        best=list(classifiers.keys())[np.argmax(f_scores)]
    #recalls
    elif metric==1:
        best=list(classifiers.keys())[np.argmax(recalls)]
    #precision
    elif metric==2:
        best=list(classifiers.keys())[np.argmax(precisions)]
    print('BEST MODEL:',best)
    best_model=list(classifiers.values())[np.argmax(f_scores)]

    #Fit best model with calibrated probabilities
     
    #final_model=CalibratedClassifierCV(best_model, cv=2, method="sigmoid")
    #Alternatively, if you do not use calibrated probabilities then comment the line above and uncomment the one below 
    final_model=best_model
    
    final_model.fit(X_train,y_train)
    #Evaluate best model
    testx=test_set[predictors].copy()
    X_test=ct.transform(testx)
    testy=test_set[[target]].values.ravel()
    final_pred=final_model.predict(X_test)
 
    if hasattr(final_model,'predict_proba'):
       y_proba=final_model.predict_proba(X_test)
       roc=roc_auc_score(testy,y_proba[:,important])
    else:
       y_proba=None
       roc=-1

  #Save results
    bestd={}
    bestd['name']=best
    bestd['model']=final_model
    bestd['index']=test_set.index
    bestd['prob']=y_proba
    bestd['y_real']=testy
    bestd['y_pred']=final_pred
    cfb=confusion_matrix(testy,final_pred)
    bestd['cf']=cfb
    print('Confusion matrix')
    print(cfb)
    p, r, f, s=precision_recall_fscore_support(testy, final_pred)
    bestd['scores']=[p,r,f,s,np.array([roc])]
    print('p=',p,'r=',r,'f=',f,'s=',s,'roc',roc)
    print('best model',bestd['model']) 
    print('best model n_features_in_',bestd['model'].n_features_in_) 
    print('final model n_features_in_',final_model.n_features_in_) 
    print('roc auc', roc )
    assert final_model.n_features_in_ == bestd['model'].n_features_in_
    if hasattr(bestd['model'],'feature_names_in_'):
       print('best model feature_names_in_',bestd['model'].feature_names_in_) 
       
    return d,bestd

def get_transformed_dataset(dataset,target,predictors, trans,scale=False, use_one_hots=True):
   nans_features, has_nan=check_nan_in_columns(dataset[predictors], trans)
   evaldataset=dataset
   if has_nan:
          evaldataset=dataset.copy()
          #Remove missing values in predictors
          for pred in predictors:
              print('pred',pred)
              sel=trans[trans['name']==pred]
              print('sel',sel) 
              if not sel['type'].values==2 :
                  print('drop', pred)
                  print('evalset', evaldataset.shape)
                  evaldataset.dropna(subset=pred,inplace=True)
                  print('evalset', evaldataset.shape)
   ct, _=createColumnTransformer(predictors,trans,scale=scale, use_one_hots=use_one_hots)
   X=ct.fit_transform(evaldataset[predictors])
   y=evaldataset[target]
   return X, y, ct

def get_standard_sets(dataset,target,predictors, trans, usePCA=False, scale=True, verbose=False, use_one_hots=True):
   nans_features, has_nan=check_nan_in_columns(dataset[predictors], trans)
   evaldataset=dataset
   if has_nan:
          evaldataset=dataset.copy()
          #Remove missing values in predictors
          for pred in predictors:
              #print('pred',pred)
              sel=trans[trans['name']==pred]
              #print('sel',sel) 
              if not sel['type'].values==2 :
                  #print('drop', pred)
                  #print('evalset', evaldataset.shape)
                  evaldataset.dropna(subset=pred,inplace=True)
                  #print('evalset', evaldataset.shape)
   train_set, test_set = train_test_split(evaldataset, test_size=0.2, random_state=42)
   if verbose:
       print('Complete train_set:',train_set)
   y_train=train_set[[target]].values.ravel()
   xt=train_set[predictors].copy()
   ct, _=createColumnTransformer(predictors,trans,scale=scale, use_one_hots=use_one_hots)
   X_train=ct.fit_transform(xt)
   if usePCA:
      pca=PCA(n_components=30)
      X_train=pca.fit_transform(X_train)
   print('X_train', X_train.shape) 
   print('n_features_in',ct.n_features_in_)
   if verbose:
       print('y (train set)=',y_train)
       print('train_set X=',X_train)
       print('train_set X.shape=',X_train.shape)
   
   testx=test_set[predictors].copy()
   X_test=ct.transform(testx)
   if usePCA:
      X_test=pca.transform(X_test)
   testy=test_set[[target]].values.ravel()
   return X_train, y_train, X_test, testy, ct, train_set, test_set 


def plot_pr_vs_t(precisions, recalls, thresholds):
   plt.plot(thresholds, precisions[:-1],"b--", label="Precision")
   plt.plot(thresholds, recalls[:-1],"g--", label="Recall")


def get_model_feature_importances(dataset,model, target,predictors,trans, scale=True, use_one_hots=True):
    if not hasattr(model,'feature_importances_'):
       return None
    print('get_model_feature_importances',predictors)
    X_train, y_train, X_test, testy, ct, train_set, test_set =get_standard_sets(dataset,target,predictors,trans, use_one_hots=use_one_hots)
    print('X_test shape', X_test.shape)
    print(model)
    print('n_features_in_',model.n_features_in_)
    #Test correctness
    y_train_pred=model.predict(X_test)
    cfb=confusion_matrix(testy,y_train_pred)
    print(cfb)
    ifei=np.flatnonzero(model.feature_importances_)
    fnames=ct.get_feature_names_out()
    print('len feature_importances_',len(model.feature_importances_))
    #print('feature_importances_',model.feature_importances_)
    fev=[(fnames[i],model.feature_importances_[i]) for i in ifei]
    fev.sort(key=lambda a: a[1], reverse=True)
    #print('feature importances',fev)
    return fev,fnames

def get_class_ratios(target,dataset, important_class):
   print('important_class',important_class)
   train_set, test_set = train_test_split(dataset, test_size=0.2, random_state=42)
   yc_all=dataset.loc[dataset[target]==important_class][target].count()
   ytotal=dataset[target].count()
   rt=float(yc_all)/ytotal
   yc_tr=train_set.loc[train_set[target]==important_class][target].count()
   ytr=train_set[target].count()
   rtr=float(yc_tr)/ytr
   yc_ts=test_set.loc[test_set[target]==important_class][target].count()
   yts=test_set[target].count()
   rts=float(yc_ts)/yts
   return rt, rtr, rts 
