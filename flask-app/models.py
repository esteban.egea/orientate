##############################################################/
#
#Copyright (c) 2023 Esteban Egea-Lopez http://girtel.upct.es/~eegea/
#
##############################################################/
from sqlalchemy import Column, Integer, String, PickleType, Boolean, DateTime, UnicodeText, LargeBinary
from sqlalchemy import ForeignKey
from sqlalchemy.orm import relationship

from database import Base
from datetime import datetime
class FeatureSelectionJob(Base):
    __tablename__ = 'feature_selection_job'
    id = Column(Integer, primary_key=True)
    dataset_id = Column(Integer, ForeignKey("dataset_configuration.id"))
    dataset = relationship("DatasetConfiguration", back_populates="jobs")
    name = Column(UnicodeText)
    start_time = Column(DateTime, nullable=False)
    end_time = Column(DateTime, nullable=True)
    completed = Column(Boolean)
    use_one_hots=Column(Boolean)
    important_class=Column(Integer)
    target = Column(PickleType)
    predictors = Column(PickleType)
    best_model=Column(PickleType)
    best_model_name=Column(UnicodeText)
    best_global=Column(PickleType)
    best_scores=Column(PickleType)
    results_path=Column(UnicodeText)
    shap_report=Column(PickleType)
    total_comb=Column(Integer)
    comb_done=Column(Integer)
    metric=Column(Integer)
    def __init__(self, dataset_id, metric=0,name=None, total_comb=0, target=None ):
        self.name = name
        self.dataset_id=dataset_id
        self.start_time =datetime.today() 
        self.end_time=None
        self.completed =False
        self.target=target
        self.predictors = None
        self.best_model = None
        self.best_model_name =None
        self.best_scores=None
        self.best_global=None
        self.results_path = "../data/results/" 
        self.total_comb=total_comb
        self.comb_done=0
        self.use_one_hots=True
        self.important_class=1
        self.metric = metric
        self.shap_report=None

    def __repr__(self):
        return f'<Job:{self.id}; Name: {self.name!r}; start_time: {self.start_time}; progress: {self.comb_done}/{self.total_comb}; finished: {self.completed}; end_time: {self.end_time}; target: {self.target}; predictors: {self.predictors}>'
    

class DatasetConfiguration(Base):
    __tablename__ = 'dataset_configuration'
    id = Column(Integer, primary_key=True)
    jobs=relationship("FeatureSelectionJob", order_by=FeatureSelectionJob.id, back_populates="dataset", cascade="delete")
    dataset_path=Column(UnicodeText)
    info_path=Column(UnicodeText)
    name=Column(UnicodeText)
    description=Column(UnicodeText)
    selection_step=Column(Integer)
    has_nan=Column(Boolean)
    upload_time = Column(DateTime, nullable=False)
    separator=Column(UnicodeText)
     
    def __init__(self, dataset_path="../data/datasets/",name="", description="", has_nan=False, selection_step=2, info_path="../data/datasets/", separator=","):
         self.name=name
         self.dataset_path=dataset_path
         self.info_path=info_path
         self.description=description
         self.selection_step=selection_step
         self.has_nan=has_nan
         self.upload_time=datetime.today()     
         self.separator=separator
 
class updateSearchFeatToDB:
    def __init__(self, job, session):
        self.job=job
        #self.log_results={}
        self.session=session
    def __call__(self,results):
         j=results['number_predictors']
         ncomb=results['number_combinations']
         cdone=results['combinations_tested']
         time=results['time']
         key=str(j)+"-"+str(ncomb) 
         #self.log_results[key]=results
         self.job.comb_done = self.job.comb_done + cdone
         self.session.commit()
         print('Updating number of combinations done', cdone)
         #print('result update', results)
   
