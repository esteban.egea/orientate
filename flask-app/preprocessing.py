##############################################################/
#
#Copyright (c) 2023 Esteban Egea-Lopez http://girtel.upct.es/~eegea/
#
##############################################################/
import numpy as np
import pandas as pd
import re
from sklearn.compose import ColumnTransformer
from sklearn.preprocessing import OneHotEncoder, StandardScaler, MinMaxScaler
from sklearn.feature_extraction import DictVectorizer
from sklearn.base import BaseEstimator, TransformerMixin
from html import escape,unescape
from os import path

class ListEncoder(BaseEstimator, TransformerMixin):
    def __init__(self, total):
        self._total=total
    def fit(self, X, y=None):
        return self
    def get_params(self,deep=True):
        return {'total':self._total}
    def set_params(self, **params): 
        self._total=params["total"]
    def get_feature_names_out(self, column):
        #print(column)
        #keys=["lel"+str(i) for i in range(1,self._total+1)]
        #return keys 
        return self._vdict.feature_names_
    def inverse_transform(self,X):
        #We will receive a numpy matrix 
        #We return a list with numbers separated by ;
        y=[]
        for i in range(X.shape[0]):
           y.append(self.decode_row(X[i,:]))
        return y
    def decode_row(self,r):
        nzi=np.nonzero(r)[0]
        if nzi.size==0:
          return ""
        nzi=nzi+1
        #t=[";".join(item) for item in nzi.astype(str)] 
        t=";".join(nzi.astype(str))
        return t
           
           
    def transform(self,X):
        #We will receive a dataframe column
        r=self.encode_column_as_dict(X.values)
        v=DictVectorizer(sparse=False, sort=False)
        x=v.fit_transform(r)
        self._vdict=v
        return x
    def encodeListAsDict(self,t):
        keys=["lel"+str(i) for i in range(0,self._total+1)]
        dic={key: 0 for key in keys}
        if type(t[0])==str:
          tl= re.split("[;,]",t[0])
          #print(tl)
          for idx, i in enumerate(tl):
            #Remove whitespaces
            ss=i.strip()
            #print(ss)
            if ss!='':
                dic["lel"+ss]=1
        return dic

    def encode_column_as_dict(self,c):
        hp=[]
        for i in c:
            #print(c.iloc[i])
            a=self.encodeListAsDict(i)
            #print(a.shape)
            hp.append(a)
        #return np.concatenate(hp, axis=0)
        return hp


def read_dataset(dataset_path, separator=","):
     dataset = pd.read_csv(dataset_path, sep=separator, parse_dates=True)
     return dataset

def load_data_direct(datafolder=None,resultfolder=None):
     if datafolder:
        DATA_DIRECTORY = path.join('..', datafolder)
     else:
        DATA_DIRECTORY = path.join('..', 'data')
     pacientes = pd.read_csv(path.join(DATA_DIRECTORY, 'dental_implant_failure.csv'), sep=',', parse_dates=True)
     if resultfolder:
         RESULT_DIRECTORY = path.join('..', 'results',resultfolder)
     else:
         RESULT_DIRECTORY = path.join('..', 'results','sedations')
     trans= pd.read_csv(path.join(DATA_DIRECTORY, 'dental_implant_failure_trans.csv'))
     ley_d, ley_r = make_legend_dicts(trans)
     
     return pacientes,  ley_d, trans, DATA_DIRECTORY, RESULT_DIRECTORY    

def load_data(dataset_path=None,info_path=None,separator=","):
     if dataset_path:
         dataset = pd.read_csv(dataset_path, sep=separator, parse_dates=True)
     if info_path:
         trans= pd.read_csv(info_path)
         ley_d, ley_r = make_legend_dicts(trans)
     
     return dataset,  ley_d, trans, ley_r   


def make_legend_dicts(trans):
   forward={}
   rev={}
   for i in trans.index.values:
        col=trans.loc[i,'name']
        #print(col)
        forward[col]={}
        rev[col]={}
        type=trans.loc[i,'type']
        forward[col]['legend']={} 
        forward[col]['type']=type
        if pd.isna(trans.loc[i,'categories']):
            continue   
        else:
            val=trans.loc[i,'categories']
            leg=trans.loc[i,'legend']
            #print(val)
            #print(leg)
            cats= re.split("[;,]",val)
            names= re.split("[;,]",leg)
            #print(cats)
            #print(names)
            for idx,v in enumerate(cats):
               forward[col]['type']=type
               forward[col]['legend'][int(v)]=names[idx]
               rev[col][names[idx]]=int(v)
   #print(forward)
   #print(rev)
   return forward,rev

   
def createColumnTransformer(columns,trans, predict=False, ohdictionary=None, verbose=False, scale=True, use_one_hots=True):
    use_oh=use_one_hots
    sel=trans[trans['name'].isin(columns)]
    if verbose:
       print('Applying transformation to dataset')
       print('columns', df.columns)
       print('selection', sel)
       print('use_one_hots', use_one_hots)
    onehots=[]
    elists=[]
    numbercols=[]
    ohdict={}
    for i in sel.index:
        col=sel.loc[i,'name']
        #print(col)
        if (sel.loc[i,'type']==1):
            if predict:
                print('predict', df.loc[:,col].to_numpy())
                a=ohdictionary[col].transform(df.loc[:,col].to_numpy().reshape(-1,1))
                hp.append(a)
                print('oh predict', a.shape)
            else:
                if not use_oh:
                   continue
                #print('One Hot')
                col=sel.loc[i,'name']
                if pd.isna(sel.loc[i,'categories']):
                    #print('not')
                    val=''
                else:
                    val=sel.loc[i,'categories']
                
                onehots.append({col:val})
        elif (sel.loc[i,'type']==2):
            #print('Encoding List')
            col=sel.loc[i,'name']
            val=int(sel.loc[i,'max-range'])
            elists.append({col:val})
        else:
            numbercols.append(sel.loc[i,'name'])
    trans=[]
    for i in elists:
       for k,v in i.items():
           trans.append((k,ListEncoder(v),[k]))
    for i in onehots:
       for k,v in i.items():
          if not v: 
             trans.append((k,OneHotEncoder(handle_unknown="ignore",sparse=False),[k]))
          else:
             tl= re.split("[;,]",v)
             #print(tl)
             cats=[int(i) for i in tl]
             trans.append((k,OneHotEncoder(categories=[cats]),[k]))
    if scale:
       for i in numbercols:
          trans.append((i,MinMaxScaler(),[i]))
          #trans.append((i,StandardScaler(),[i]))

    ct=ColumnTransformer(trans,remainder='passthrough', sparse_threshold=0)
    return ct, ohdict
   


def create_dataset_table(dataset, legend_dict):
    table=dataset.copy()
    for k,v in legend_dict.items():
        #print(k)
        if legend_dict[k]["legend"]:
            for m,n in legend_dict[k]["legend"].items():
                #print('k',k,'m',m,'n',n)
                table.replace({k:m},n,inplace=True)
                #print(table[k].head())
    return table


def make_feat_val_dict(df, d):
    fvd={}
    co=[]
    for i in df:
        if i in d.keys():
             fvd[i]=d[i]['legend']
        else:
            co.append(i)
    return fvd,co

#Replace the numerical values with the legend for better readability of results
def subs_ley(df, d):
    #print(d)
    for i in df.columns:
        if i in d.keys():
           df=df.replace(d[i]['legend'])
    
    print(df)
    

    
def number_of_classes(df,trans):
    sel=trans[trans['name'].isin(df.columns)]
    #print(sel)
    for i in sel.index:
        col=sel.loc[i,'name']
        #print(col)
        if (sel.loc[i,'type']==1):
            tl= re.split("[;]",sel.loc[i,'categories'])
            na=df.isnull().values.any()
            return tl, na
            #print('cat',len(ohe.categories_))
            #print('onehots',onehots)
        elif (sel.loc[i,'type']==2):
            #col=sel.loc[i,'n']
            val=int(sel.loc[i,'max-range'])
            #print('predefined categories:', val)
            return range(val), False
           
        else:
            return -1, True



#Return the number of nans in each column of the datasets, nans in tooth lists are excluded with excludeLists by default
def check_nan_in_columns(df, trans, excludeLists=True):
    nans={}
    has_nan=False
    for i in df.columns:
        #print(i,df[i].isna().sum())
        #nans.append(df[i].isna().sum())
        if excludeLists:
              row=trans[trans['name']==i]
              #print('row',row) 
              if row['type'].values!=2:
                  nans[i]=df[i].isna().sum()
                  if nans[i]>0:
                     has_nan=True
                       
        else:
           nans[i]=df[i].isna().sum()
           if nans[i]>0:
               has_nan=True
    return nans, has_nan

def np_html(a, classes_dict):
    html = [f"<table class='table table-bordered'>"]
    html +='<thead><tr><th></th>'
    rows, columns = a.shape
    names=[]
    for k,v in classes_dict.items():
         names.append(v)
    #html += (f"<th>{j}" for j in range(columns))
    html += (f"<th scope='col'>Prediction {names[j]}" for j in range(columns))
    html +='</tr></thead>'
    for i in range(rows):
        html.append(f"<tr>")
        html.append(f"<th> Real {names[i]}</th>")
        for j in range(columns):
            val = a[i, j]
            html.append('<td>')
            html.append(escape(f"{val:.2f}" if a.dtype == float else f"{val}"))
            html.append("</td>")
        html.append("</tr>")
    html.append("</table>")
    return "".join(html)

def classes_to_dict(classes_pred,nan_pred, cln_pred):
    classes_dict={}
    nclasses=len(classes_pred)
    print('classes_pred',classes_pred)
    if (nan_pred):
      nclasses -=1
      for i in range(len(classes_pred)-1):
         v=classes_pred[0][i]
         classes_dict[v]=cln_pred[v]
         
    else:
      for i in range(len(classes_pred)):
         v=classes_pred[i]
         print(v)
         print(cln_pred)  
         print(cln_pred['legend'][int(v)])
         classes_dict[v]=cln_pred['legend'][int(v)]
    return classes_dict, nclasses

