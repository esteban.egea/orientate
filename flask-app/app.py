##############################################################/
#
#Copyright (c) 2023 Esteban Egea-Lopez http://girtel.upct.es/~eegea/
#
##############################################################/


import numpy as np
import zipfile
import re
import pickle
import traceback
from datetime import datetime

import pandas as pd
import os
import sys
import tempfile
from os import path
import multiprocessing as mp
from joblib import dump, load

from IPython.display import HTML


from bokeh.plotting import figure
from bokeh.models import  HoverTool

from bokeh.resources import CDN
from bokeh.embed import components 
from sklearn.tree import DecisionTreeClassifier

from preprocessing import  check_nan_in_columns, number_of_classes,  np_html, subs_ley, classes_to_dict, load_data, createColumnTransformer, make_feat_val_dict,  create_dataset_table, make_legend_dicts, read_dataset
from classifiers import  generate_classifiers, evaluate_classifiers_transformer, get_model_feature_importances, get_class_ratios,get_standard_sets
from featsel import computeCombinations,search_features_selection, searchToFile, computeAllCombinations
from database import db_session
from sqlalchemy import select
from models import FeatureSelectionJob, updateSearchFeatToDB, DatasetConfiguration 
from reports import sel_feat_report, create_shap_report, mpl_to_base64, make_histogram_plot, get_data_for_shap_report 
from flask import Flask, render_template, request, session, send_from_directory, redirect, url_for, abort, flash, send_file
from flask_caching import Cache
from werkzeug.utils import secure_filename


#Change to RedisCache if necessary


app = Flask(__name__)
app.secret_key=os.urandom(12)
#LOAD DATA 
tmpdir = tempfile.mkdtemp()
UPLOAD_FOLDER = '../data/datasets'
ALLOWED_EXTENSIONS = {'txt', 'csv'}

app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
savedir = path.join('.', 'files')
filename_model = path.join(savedir, 'model.joblib')
filename_confusion  = path.join(savedir, 'confusion.joblib')
filename_report = path.join(savedir, 'report.joblib')
filename_test_set = path.join(savedir, 'test_set.joblib')

running_flask=True


#ui_lang='es/'
ui_lang='en/'
config_cache = {
    "CACHE_TYPE": "SimpleCache",  # Flask-Caching related configs
    "CACHE_DEFAULT_TIMEOUT": 3600
}

cache = Cache(app, config=config_cache)
ctx=mp.get_context('spawn')


@app.teardown_appcontext
def shutdown_session(exception=None):
    db_session.remove()


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@app.route('/')
def select_dataset():
    datasets=DatasetConfiguration.query.all()
    tname=ui_lang+'select_dataset.html'
    return render_template( tname, datasets=datasets)
  
@app.route('/delete_dataset/<dataset_id>')
def delete_dataset(dataset_id):
    job=DatasetConfiguration.query.filter_by(id=int(dataset_id)).first()
    os.remove(job.dataset_path+str(job.id)+".csv")
    os.remove(job.info_path+str(job.id)+"_info.csv")
    db_session.delete(job)
    db_session.commit()
    return redirect(location=url_for('select_dataset'))
  

@app.route('/process_dataset/', methods=['POST'])
def process_dataset():
    if request.method == 'POST':
        # check if the post request has the file part
        if 'dataset' not in request.files:
            flash('No dataset uploaded')
            return redirect(request.url)
        if 'dataset_info' not in request.files:
            flash('No dataset info uploaded')
            return redirect(request.url)
        dataset = request.files['dataset']
        dataset_info = request.files['dataset_info']
        separator=","
        if request.form['sep'] == 'colon':
            separator=";"
        # If the user does not select a file, the browser submits an
        # empty file without a filename.
        if dataset.filename == '':
            flash('No selected dataset file')
            return redirect(request.url)
        if dataset_info.filename == '':
            flash('No selected dataset info file')
            return redirect(request.url)
        if dataset and dataset_info and allowed_file(dataset.filename) and allowed_file(dataset_info.filename):
            print('dataset',dataset,'dataset_info',dataset_info)
            try:
                 data=read_dataset(dataset,separator )
            except Exception as inst:
                tname=ui_lang+'error_dataset.html'
                print(type(inst))    # the exception instance
                print(inst.args) 
                return render_template( tname, error='dataset', additional=inst.args)
            try:
                 trans=read_dataset(dataset_info)
            except Exception as inst:
                tname=ui_lang+'error_dataset.html'
                print(type(inst))    # the exception instance
                print(inst.args) 
                return render_template( tname, error='dataset info', additional=inst.args)
            try:
                ley_d, ley_r = make_legend_dicts(trans)
            except Exception as inst:
                tname=ui_lang+'error_dataset.html'
                print(type(inst))    # the exception instance
                print(inst.args) 
                return render_template( tname, error='dataset info', additional='Please check that the dataset description information complies with the format. '+inst.args)
            dname=request.form["dname"]                 
            description=request.form["description"]
            if data.isnull().sum().sum() > 0:
               has_nan=True
            else:
               has_nan=False
            datadb=DatasetConfiguration(name=dname,description=description,has_nan=has_nan, separator=separator)  
            db_session.add(datadb)
            db_session.commit()
            data.to_csv(os.path.join(app.config['UPLOAD_FOLDER'], str(datadb.id)+'.csv'),sep=separator, index=False)
            trans.to_csv(os.path.join(app.config['UPLOAD_FOLDER'], str(datadb.id)+'_info.csv'), index=False)
            tname=ui_lang+'uploaded_dataset.html'
            nans_features, has_nan=check_nan_in_columns(data, trans)
            #print(nans_features)
            return render_template(tname,  nans_features=nans_features)


def load_data_and_cache(dataset_id):
    data=DatasetConfiguration.query.filter_by(id=int(dataset_id)).first()
    if data:
       dpath=data.dataset_path+str(dataset_id)+'.csv'
       ipath=data.info_path+str(dataset_id)+'_info.csv'
       print('seo',data.separator)
       dataset,  ley_d, trans, ley_r  = load_data(dataset_path=dpath, info_path=ipath,separator=data.separator)
       cache.set("dataset",dataset)
       cache.set("dataset_id",dataset_id)
       cache.set("trans",trans)
       cache.set("ley_d",ley_d) 
       cache.set("ley_r",ley_r)
       cache.set("dataset_name",data.name)
       cache.set("selection_step", data.selection_step)
       return True
    else:
      return False 

@app.route('/show_dataset/<dataset_id>')
def show_dataset(dataset_id):
    if load_data_and_cache(dataset_id):
       dataset=cache.get("dataset")
       ley_d=cache.get("ley_d")
       pacientes_table=create_dataset_table(dataset,ley_d)
       pacientes_t = pacientes_table.to_html(justify='center', index=False ,table_id="myTable", border="0", classes="table-striped hover").replace('<th>', '<th style = "background-color: #007bff; color: white";>' )
       tname=ui_lang+'index.html'
       return render_template(
         tname,
         dataset_name=cache.get("dataset_name"),
         datatables=True,
         f_condition=False,
         pacientes=HTML(style + pacientes_t)
       )
    else: 
       tname=ui_lang+'error.html'
       return render_template(tname, error='Dataset not found in the database')



@app.route('/evaluate_form/<type>')
def evaluate_form(type):
    if type=='evaluate':
       print('evaluate')
    else:
       print('select')
    dataset=cache.get("dataset")
    if dataset is None:
         return redirect(location=url_for('select_dataset'))
    ley_d=cache.get("ley_d")
    features =dataset.columns.sort_values().values.tolist()
    
    #Only features in ley_d.keys() can be used as targets for classificaction predictions (have well defined classes, others have to be used with regressors)
    features_pred=list(ley_d.keys())
    features_pred.sort()
    tname=ui_lang+'eval_form.html'
    return render_template(
                tname,
		prepare_classification='/prep_class/',
		features=features,
		features_pred=features_pred,
                type=type) 

@app.route('/prepare_classification/', methods=['GET', 'POST'])
def prepare_classification():
    feat_list = request.form.getlist("classification")
    feat_pred = request.form.get("feature_predicted_form")
    type=request.form.get("type")
    dataset=cache.get("dataset")
    if dataset is None:
         return redirect(location=url_for('select_dataset'))
    trans=cache.get("trans")
    ley_d=cache.get("ley_d")
    print(feat_pred)
    classes_pred, nan_pred=number_of_classes(dataset[[feat_pred]],trans)
    cln_pred=ley_d[feat_pred]
    classes_dict, nclasses =classes_to_dict(classes_pred, nan_pred, cln_pred) 
    nans_features, has_nan=check_nan_in_columns(dataset[feat_list], trans)
    ratios=[]
    for i,v in enumerate(classes_pred):
        rt, rtr, rts = get_class_ratios(feat_pred,dataset, int(classes_pred[i]))
        ratios.append(rt) 
 
    if type=="evaluate":
        tname=ui_lang+'summary_pre_eval.html'
        return render_template(
           tname,
	   classes_dict=classes_dict,
	   feat_list=feat_list,
	   feat_pred=feat_pred,
           nclasses=nclasses,
           nan_pred=nan_pred,
	   nans_features=nans_features,
           ratios=ratios
	   )
    else:
        selection_step=cache.get("selection_step")
        ncomb=computeCombinations(len(feat_list),selection_step)
        ncomball=computeAllCombinations(len(feat_list))
        #print('ncomball',ncomball)
        if ncomb > 10000:
          return render_template(ui_lang+'ncomb_go_back.html', predictors=len(feat_list), ncomb=ncomb)
        else:
          tname=ui_lang+'summary_pre_feat_sel.html'
          return render_template(
           tname,
	   classes_dict=classes_dict,
	   feat_list=feat_list,
	   feat_pred=feat_pred,
           nclasses=nclasses,
           nan_pred=nan_pred,
	   nans_features=nans_features,
           ncomb=ncomb,
           ncomball=ncomball,
           ratios=ratios
	   )

def run_spawn_search(dataset_id, selection_step,dataset, trans, ley_d, feat_list, feat_pred, strategy,classes,important, jobname,ncomb,full=False, use_one_hots=True, metric=0):
    classes_pred, nan_pred=number_of_classes(dataset[[feat_pred]],trans)
    metric=metric
    job=FeatureSelectionJob(dataset_id,metric=metric,name=jobname,total_comb=ncomb, target=feat_pred)
    db_session.add(job)
    db_session.commit()
    print("job",job)
    sys.stdout = open(job.results_path+str(job.id) + ".out", "a")
    sys.stderr = open(job.results_path+str(job.id) + "_error.out", "a")
    update_cb=updateSearchFeatToDB(job, db_session)
    #stf=searchToFile(folderpath="../results/etest6")
    #callbacks=[stf,update_cb]
    callbacks=[update_cb]
    

    use_oh=use_one_hots
    predictors=feat_list
    if strategy=='none':
        evaldataset=dataset
    else:
       if strategy=='remove':
          evaldataset=dataset.dropna(subset=feat_pred)
       else:
          evaldataset=dataset.fillna(value=float(strategy), columns=feat_pred)

    print('selection_step',selection_step)
    globalbest, log =search_features_selection(evaldataset, predictors, selection_step,feat_pred, trans,len(classes_pred),important,metric, classifiers_fn=generate_classifiers, progress_callback=callbacks, fullSearch=full, use_one_hots=use_oh)
    gbi=globalbest['gbi']
    job.important_class=important
    job.use_one_hots=use_oh
    job.best_model_name=globalbest["name"][gbi]
    job.best_model=globalbest["model"][gbi]
    job.predictors=globalbest["predictors"][gbi]
    job.best_scores= globalbest["scores"][gbi]
    job.best_global=globalbest
    job.completed=True
    job.end_time=datetime.today()
    with open(job.results_path+str(job.id)+"-log.pkl", 'wb') as f:
         pickle.dump(log, f, pickle.HIGHEST_PROTOCOL)
     
    db_session.commit()
    print('Global best combination', globalbest["name"][gbi],globalbest["predictors"][gbi],globalbest["scores"][gbi])


@app.route('/exec_feature_sed2/')
def exec_feature_sed2():
  # full_predictors=["EDAD PRIMERA SEDACIÓN","PATOLOGIA","HÁBITOS DE HIGIENE","DIENTES CON CARIES","PLACA",
  #             "AFECTACIÓN PULPAR","DIENTES CON AFECTACIÓN PULPAR","AUSENCIAS", "OBTURACIONES REALIZADAS","LISTA DIENTES OBTURADOS",
  #             "PPD", "PULPECTOMÍAS", "LISTA DIENTES CON PULPECTOMÍAS", "PULPOTOMÍAS","LISTA DIENTES CON PULPOTOMÍAS","SELLADORES",
  #             "LISTA DIENTES CON SELLADORES","APLICACIÓN DE FLÚOR","EXTRACCIONES","LISTA DIENTES EXTRAÍDOS","REVISIÓN POST-SEDACIÓN","SEGUIMIENTO DE PREVENCIÓN",
  # #            #"COMPORTAMIENTO EN LAS CITAS","PRESENCIA DE CARIES",
  #              "MOTIVACIÓN",
  #             "DIENTES T PULPAR PRIMERA SEDACIÓN","EXOS POR PATOLOGÍA","LISTA EXTRAÍDOS POR PATOLOGÍA",
  #             "T PULPAR PRIMERA SEDACIÓN","SEDACIÓNLT6","SEDACIÓN6TO12","SEDACIÓNGT12"]
   full_predictors=["FirstSedationAge","Healthy","HygieneHabits","NumCaries","Plaque","PulparInvolvement","NumPulparInvolvement","Missing","Fillings","ListFillings","PPD","Pulpectomies","ListPulpectomies","Pulpotomies","ListPulpotomies","Sealants","ListSealants","FluorideApplication","Extractions","ListExtractions","PostSedationCheck","PreventionTracking","Motivation","NumPulparTreatmentsFirstSedation","NumExosPathology","ListExosPathology","PulparTreatmentFirstSedation","SedationBefore6","Sedation6To12","SedationAfter12"]

   #full_predictors=["EDAD PRIMERA SEDACIÓN","PATOLOGIA","HÁBITOS DE HIGIENE","PRESENCIA DE CARIES","DIENTES CON CARIES","LISTA DIENTES OBTURADOS"]
  # pred_eleven=["EDAD PRIMERA SEDACIÓN","PATOLOGIA","DIENTES CON CARIES","PLACA","NUM DIENTES OBTURADOS","NUM DIENTES CON SELLADORES","SEGUIMIENTO DE PREVENCIÓN","MOTIVACIÓN","DIENTES T PULPAR PRIMERA SEDACIÓN","EXOS POR PATOLOGÍA","NUM EXTRAÍDOS POR PATOLOGÍA"]
  # pred_twentytwo = ["EDAD PRIMERA SEDACIÓN","PATOLOGIA","HÁBITOS DE HIGIENE","DIENTES CON CARIES","PLACA","DIENTES CON AFECTACIÓN PULPAR","DIENTES EXTRAÍDOS","NUM DIENTES OBTURADOS","NUM DIENTES CON PULPECTOMÍAS","NUM DIENTES CON PULPOTOMÍAS","NUM DIENTES ENDODONCIADOS","NUM DIENTES CON SELLADORES","APLICACIÓN DE FLÚOR","SEGUIMIENTO DE PREVENCIÓN","MOTIVACIÓN","DIENTES T PULPAR PRIMERA SEDACIÓN","EXOS POR PATOLOGÍA","NUM EXTRAÍDOS POR PATOLOGÍA"]
  # pred_eleven_list=["EDAD PRIMERA SEDACIÓN","PATOLOGIA","DIENTES CON CARIES","PLACA","LISTA DIENTES OBTURADOS","LISTA DIENTES CON SELLADORES","SEGUIMIENTO DE PREVENCIÓN","MOTIVACIÓN","DIENTES T PULPAR PRIMERA SEDACIÓN","EXOS POR PATOLOGÍA","LISTA EXTRAÍDOS POR PATOLOGÍA"]
  # pred_twentytwo_list = ["EDAD PRIMERA SEDACIÓN","PATOLOGIA","HÁBITOS DE HIGIENE","DIENTES CON CARIES","PLACA","DIENTES CON AFECTACIÓN PULPAR","DIENTES EXTRAÍDOS","LISTA DIENTES OBTURADOS","LISTA DIENTES CON PULPECTOMÍAS","LISTA DIENTES CON PULPOTOMÍAS","LISTA DIENTES ENDODONCIADOS","LISTA DIENTES CON SELLADORES","APLICACIÓN DE FLÚOR","SEGUIMIENTO DE PREVENCIÓN","MOTIVACIÓN","DIENTES T PULPAR PRIMERA SEDACIÓN","EXOS POR PATOLOGÍA","LISTA EXTRAÍDOS POR PATOLOGÍA"]
  # #full_predictors=pred_eleven
   #full_predictors=pred_eleven_list
   #full=True
   #full_predictors=pred_twentytwo_list
   #full_predictors=pred_twentytwo
   full=False
   #if type=="el":
   #   full_predictors=pred_eleven
   #   full=True
   #else:
   #   full_predictors=pred_twentytwo
   print('predictors to search', len(full_predictors), full_predictors)
   name="full  "
   use_one_hots=False
   #use_one_hots=True
   if not use_one_hots:
      name=name+" no oh"
   print(name)
   #sed=2
   #target="SEDACIÓN "+str(sed)
   target="SecondSedation"
   classes=2
   important=1
   jobname=name
   metric=0
   return exec_feature_select(feat_pred=target, feat_list=full_predictors, strategy="none", classes=classes, important=important, jobname=jobname, full=full, use_one_hots=use_one_hots, metric=0) 
@app.route('/exec_feature_select/', methods=["GET", "POST"])
def exec_feature_select(feat_pred=None,feat_list=None, strategy=None, classes=None, important=None,jobname=None, full=None, use_one_hots=None,metric=None):
    dataset=cache.get("dataset")
    if dataset is None:
         return redirect(location=url_for('select_dataset'))
    dataset_id=cache.get("dataset_id")
    trans=cache.get("trans")
    ley_d=cache.get("ley_d")
    selection_step=cache.get("selection_step")
    if feat_list is None:
       fl = request.form.getlist("feat_list")
       feat_list=eval(fl[0])
       print('fl',fl)
    if feat_pred is None:
       feat_pred = request.form.get("feat_pred")
    if strategy is None:
       strategy = request.form.get("strategy", default='unsent')
    if classes is None:
       classes=int(request.form.get("nclasses"))
    if important is None:
       important = int(request.form.get("important"))
    if jobname is None:
       jobname = request.form.get("jobname")
    if full is None:
        aux= int(request.form.get("full"))
        if aux==1:
            full=True
        else:
            full=False
    if use_one_hots is None:
        aux= int(request.form.get("use_one_hots"))
        if aux==1:
           use_oh=True
        else:
           use_oh=False
    else:
        use_oh=use_one_hots 
    if full:
        ncomb=computeAllCombinations(len(feat_list))
    else:
        ncomb=computeCombinations(len(feat_list),2)
    if metric is None:
        metric= int(request.form.get("metric"))
    if strategy=='none':
        evaldataset=dataset
    else:
        #TODO: too many copies of the dataset. This should be optimized. It should not happen if missing values are removed/imputed when the dataset is uploaded
       evaldataset=dataset.copy()
       if strategy=='remove':
          evaldataset=dataset.dropna(subset=feat_pred)
       else:
          evaldataset=dataset.fillna(value=float(strategy), columns=feat_pred)
    process=ctx.Process(target=run_spawn_search,args=(dataset_id, selection_step,evaldataset,trans, ley_d, feat_list,feat_pred,strategy,classes,important,jobname,ncomb,full, use_oh,metric))
    process.start()
    return redirect(location=url_for('show_feat_sel_jobs'))
@app.route('/feat_sel_jobs/')
def show_feat_sel_jobs():
    dataset=cache.get("dataset")
    if dataset is None:
         return redirect(location=url_for('select_dataset'))
    dataset_id=cache.get("dataset_id")
    dataset_name=cache.get("dataset_name")
    #jobs=FeatureSelectionJob.query.all()
    jobs=FeatureSelectionJob.query.filter_by(dataset_id=dataset_id)
    pending =[j for j in jobs if not j.completed]
    completed =[j for j in jobs  if j.completed]
    tname=ui_lang+'feat_sel_jobs.html'
    return render_template(
             tname,dataset_name=dataset_name,pending=pending, completed=completed)
@app.route('/reevaluate_classifiers/<id>/<model_id>')
def reevaluate_classifiers(id, model_id):
    job=FeatureSelectionJob.query.filter_by(id=int(id)).first()
    if job is None:
       abort(404)
    best=job.best_global
    gbi=int(model_id)
    name=best['name'][gbi]
    target=job.target
    predictors=best['predictors'][gbi]
    important_class=1
    dataset=cache.get("dataset")
    rt, rtr, rts = get_class_ratios(target,dataset,important_class )
    ratios=[1.0-rt, rt]
    return exec_eval_classification(feat_list=predictors,feat_pred=target,strategy='none',nclasses=2,yt=None, xt=None, classes_dict=None, important=important_class, classes_pred=None,rt=ratios, use_one_hots=job.use_one_hots, metric=job.metric)

@app.route('/predict_samples/<id>/<model_id>')
def predict_samples(id,model_id):
    job=FeatureSelectionJob.query.filter_by(id=int(id)).first()
    if job is None:
       abort(404)
    best=job.best_global
    gbi=int(model_id)
    name=best['name'][gbi]
    target=job.target
    predictors=best['predictors'][gbi]
    numbers=[]
    selects=[] 
    lists=[]
    ley_d=cache.get("ley_d")
    for i in predictors:
         nt=i
         if ley_d[i]['type']==0:
            numbers.append({'name':i,'label':nt})
         elif ley_d[i]['type']==1:
            selects.append({'name': i, 'options':ley_d[i]['legend'], 'label':nt})
         else:
            lists.append({'name':i,'label':nt})
  
    tname=ui_lang+'predict_samples.html'    
    return render_template(tname,target=target, id=id, model_id=model_id,selects=selects,numbers=numbers,lists=lists, feat_list=predictors)


@app.route('/obtain_prediction/', methods=['GET', 'POST'])
def obtain_prediction():
    id=int(request.form.get('id'))
    job=FeatureSelectionJob.query.filter_by(id=int(id)).first()
    if job is None:
       abort(404)
    trans=cache.get("trans")
    if trans is None:
         return redirect(location=url_for('select_dataset'))
    ley_d=cache.get("ley_d")
    best=job.best_global
    gbi=int(request.form.get('model_id'))
    name=best['name'][gbi]
    target=job.target
    predictors=best['predictors'][gbi]
    model=best['model'][gbi]
    use_one_hots=job.use_one_hots
    feat_list=re.split("[,]",request.form.getlist("feat_list")[0])
    d={} 
    for i in feat_list[:-1]:
        if ley_d[i]['type']==2:
            d[i]=request.form.get(i)
        else:
            d[i]=float(request.form.get(i))
           
    
    df=pd.DataFrame(d, index=[0])
    ct, _=createColumnTransformer(predictors,trans,scale=True, use_one_hots=use_one_hots)
    X=ct.fit_transform(df)
    value=model.predict(X) 
    prob=model.predict_proba(X)
    tname=ui_lang+'pred.html'
    sr=job.shap_report[gbi]
    sv=sr.explainer(X)
    waterfall = mpl_to_base64(sr.get_waterfall_sample(sv[...,1][0], max_display=8))
    return render_template(tname,value=value, prob=prob,waterfall=waterfall)

def translate_shap_names(ylabels,t):
   
   n=[]
   for s in ylabels:
     if s in t.keys():
        n.append(t[s])
     else:
        n.append(s)
   return n


def get_shap_report(job, id, model_id, rescaleDisplay=False):
    dataset=cache.get("dataset")
    print(dataset.shape)
    if dataset is None:
         return redirect(location=url_for('select_dataset'))
    trans=cache.get("trans")
    ley_d=cache.get("ley_d")
    gbi=int(model_id)
    important_class=job.important_class  
    best=job.best_global
    #gbi=best["winners"][0]
    #print('winners',best['winners']) 
    #print('gbi',gbi) 
    model=best['model'][gbi]
    #print('model',model)
    name=best['name'][gbi]
    target=job.target
    predictors=best['predictors'][gbi]
    use_oh=job.use_one_hots
    rt, rtr, rts = get_class_ratios(target,dataset,important_class )
    if job.shap_report is not None:
       if gbi in job.shap_report:
           shap_report=job.shap_report[gbi]
       else:
         try:
             name, predictors, target, rt,  shap_report=create_shap_report(job,dataset,trans, ley_d,gbi, use_one_hots=use_oh, important_class=important_class)
             aux=job.shap_report
             aux[gbi]=shap_report
             job.shap_report=aux
             db_session.commit()
         except:
            traceback.print_exc()
            raise
    else:
        try:
          name, predictors, target, rt,  shap_report=create_shap_report(job,dataset,trans, ley_d,gbi, use_one_hots=use_oh, important_class=important_class)
          aux={}
          aux[gbi]=shap_report
          job.shap_report=aux
          db_session.commit()
        except:
            raise
    if rescaleDisplay:
       ct, _=createColumnTransformer(predictors,trans,scale=True, use_one_hots=use_oh)
       print(dataset.shape)
       data=dataset.dropna(subset=predictors)
       print(data.shape)
       X_train, y_train, X_test, testy, ct, train_set, test_set =  get_standard_sets(data,target,predictors, trans, usePCA=False, scale=True, verbose=False, use_one_hots=use_oh)
       ct.fit(data[predictors])
       print(ct)
       shap_report.set_X_display(ct, use_one_hots=use_oh)
    return name, predictors, target, rt, shap_report
@app.route('/show_shap_samples/<id>/<model_id>', methods=['GET'])
def show_shap_samples(id, model_id):
    job=FeatureSelectionJob.query.filter_by(id=int(id)).first()
    if job is None:
       abort(404)
    dataset=cache.get("dataset")
    #Get reports
    try:
       name, predictors, target, rt, shap_report = get_shap_report(job, int(id), int(model_id))
    except Exception as inst:
       tname=ui_lang+'error.html'
       print(type(inst))    # the exception instance
       print(inst.args) 
       return render_template( tname, error='Error creating SHAP report', additional=inst.args)
    force=[]
    report=[]
    waterfall=[]
    max_range = int(request.args["max_range"])
    if shap_report is not None:
       y_real=dataset[target].values
       y_pred=shap_report.model.predict(shap_report.X)
       report.append(y_pred[0:max_range])
       report.append(y_real[0:max_range])
       for i in predictors:
         report.append(dataset[i].values[0:max_range])
       ev=[1.0-rt,rt]
       force_all=shap_report.get_force_all()
       force=shap_report.get_force()
       waterfall = [mpl_to_base64(i) for i in shap_report.get_waterfall(max_display=6,max_range=max_range)]
       decplot=None
    tname=ui_lang+'sample_shap_report.html'
    print(report)
    #if ui_lang=='en/':
        #predictors=pt
    
    return render_template(tname,name=name, predictors=predictors, target=target,  rt=rt, id=id, model_id=model_id,  shap_expected_values=ev, force_all=force_all, force=force, report=report, waterfall=waterfall, decplot=decplot) 

@app.route('/download_feat_report/<id>/<model_id>')
def download_feat_report(id, model_id):
    job=FeatureSelectionJob.query.filter_by(id=int(id)).first()
    if job is None:
       abort(404)
    load_data_and_cache(job.dataset_id)
    dataset=cache.get("dataset")
    if dataset is None:
         return redirect(location=url_for('select_dataset'))
    trans=cache.get("trans")
    ley_d=cache.get("ley_d")
    gbi=int(model_id)
    important_class=job.important_class  
    best=job.best_global
    #gbi=best["winners"][0]
    #print('winners',best['winners']) 
    #print('gbi',gbi) 
    model=best['model'][gbi]
    #print('model',model)
    name=best['name'][gbi]
    target=job.target
    predictors=best['predictors'][gbi]
    use_oh=job.use_one_hots
    rt, rtr, rts = get_class_ratios(target,dataset,important_class )
    data=get_data_for_shap_report(job,dataset,trans, ley_d,gbi, use_one_hots=use_oh, important_class=important_class)
    print('data',data)
    mname=next(tempfile._get_candidate_names())
    mpath=path.join(tmpdir,mname)
    with open(mpath+".pkl", 'wb') as f:
         pickle.dump(data,f)
    print('dowloading from', mpath,'dir',tmpdir) 
    return send_file( mpath+".pkl", as_attachment=True)


  
@app.route('/show_details_feat_report/<id>/<model_id>')
def show_details_feat_report(id, model_id):
    job=FeatureSelectionJob.query.filter_by(id=int(id)).first()
    if job is None:
       abort(404)
    load_data_and_cache(job.dataset_id)
    dataset=cache.get("dataset")
    if dataset is None:
         return redirect(location=url_for('select_dataset'))
    #Get reports
    try:
      name, predictors, target, rt, shap_report = get_shap_report(job, int(id), int(model_id), rescaleDisplay=True)
    except Exception as inst:
       traceback.print_exc()
       tname=ui_lang+'error.html'
       print(type(inst))    # the exception instance
       print(inst.args) 
       return render_template( tname, error='Error creating SHAP report', additional=inst.args)

    #Get stats report

    y_pred=shap_report.model.predict(shap_report.X)
    stat_rep=""
    shap_summary=None
    bsdp=[]
    beeswarm=None
    bar_cohort=None
    bar_cluster=None
    use_shap=False
    max_range=len(y_pred)
    if shap_report is not None:
       shap_summary=mpl_to_base64(shap_report.get_summary())
       #barbase=mpl_to_base64(bar)
       bsdp = [mpl_to_base64(i) for i in shap_report.get_dependence()]  
       beeswarm=mpl_to_base64(shap_report.get_beeswarm(max_display=8))
       bar_cohort=mpl_to_base64(shap_report.get_bar_cohort(max_display=8))
       bar_cluster=mpl_to_base64(shap_report.get_bar_cluster())
       #force=mpl_to_base64(shap_report.force_all)
       force_all=shap_report.get_force_all()
       use_shap=True 
       #if hasattr(shap_report,'expected_value'):
       #   ev=shap_report.expected_value
       #else:
       ev=[1.0-rt,rt]
    tname=ui_lang+'info_sel_feat_report.html'
    return render_template(tname,name=name, predictors=predictors, target=target,  rt=rt, id=id, model_id=model_id, pdp=shap_summary, bar=None,sdp=bsdp, beeswarm=beeswarm, bar_cohort=bar_cohort, bar_cluster=bar_cluster,use_shap=use_shap, shap_expected_values=ev, force_all=force_all,stat_rep=stat_rep, max_range=max_range) 

@app.route('/delete_feat_report/<id>')
def delete_feat_report(id):
    job=FeatureSelectionJob.query.filter_by(id=int(id)).first()
    db_session.delete(job)
    db_session.commit()
    return redirect(location=url_for('show_feat_sel_jobs'))

@app.route('/show_feat_report/<id>')
def show_feat_report(id):
    job=FeatureSelectionJob.query.filter_by(id=int(id)).first()
    if job is None:
       abort(404)
    load_data_and_cache(job.dataset_id)
    dataset=cache.get("dataset")
    if dataset is None:
         return redirect(location=url_for('select_dataset'))
    trans=cache.get("trans")
    name, predictors, target,  script_bmodels, div_bmodels, summa, fe_list, has_importances, tplots, rt=sel_feat_report(job,dataset,trans) 
    tname=ui_lang+'feat_report.html'
    return render_template(tname,name=name, predictors=predictors, target=target, script_bmodels=script_bmodels, div_bmodels=div_bmodels, summa=summa, fe_list=fe_list, has_importances=has_importances, tplots=tplots, rt=rt, id=id) 
   
@app.route('/exec_eval_classification/', methods=['GET', 'POST'])
def exec_eval_classification(feat_list=None, feat_pred=None, strategy=None, nclasses=None, yt=None, xt=None, classes_dict=None, important=None, classes_pred=None, rt=None,  use_one_hots = None, verbose=True, metric=None):
    dataset=cache.get("dataset")
    if dataset is None:
         return redirect(location=url_for('select_dataset'))
    trans=cache.get("trans")
    ley_d=cache.get("ley_d")
    
    if feat_list is None:
        fl = request.form.getlist("feat_list")
        feat_list=eval(fl[0])
        if verbose:
            print('fl',fl,'feat_list', feat_list)
    if feat_pred is None:
        feat_pred = request.form.get("feat_pred")
    if strategy is None:
        strategy = request.form.get("strategy", default='unsent')
    if nclasses is None:
       classes=int(request.form.get("nclasses"))
    else:
       classes=nclasses
    if important is None:
       important = request.form.get("important")
    #print('classes', classes, 'feat_list', feat_list[0],'feat_pred', feat_pred)
    if classes_dict is None:
       classes_pred, nan_pred=number_of_classes(dataset[[feat_pred]],trans)
       cln_pred=ley_d[feat_pred]
       classes_dict, nc =classes_to_dict(classes_pred, nan_pred, cln_pred)
    if rt is None:
       rt=request.form.get("ratios")
       ratios=eval(rt) 
    else:
       ratios=rt
    if use_one_hots is None:
        aux= int(request.form.get("use_one_hots"))
        if aux==1:
           use_oh=True
        else:
           use_oh=False
    else:
       use_oh=use_one_hots 
    if verbose:
       print('classes_dict:',classes_dict) 
    if metric is None:
        metric= int(request.form.get("metric"))

    cache.delete("models")       
    cache.delete("ohd")       
    important_class=1
    for i in range(len(classes_pred[0])):
        if important==classes_pred[0][i]:
           if verbose:
              print('important class', important, classes_pred[0][i])
           important_class=i
    predictors=feat_list
    if strategy=='none':
        evaldataset=dataset
    else:
        #TODO: too many copies of the dataset. This should be optimized. It should not happen if missing values are removed/imputed when the dataset is uploaded
       evaldataset=dataset.copy()
       if strategy=='remove':
          evaldataset=dataset.dropna(subset=feat_pred)
       else:
          evaldataset=dataset.fillna(value=float(strategy), columns=feat_pred)

    
    ct, _=createColumnTransformer(predictors,trans, use_one_hots=use_oh)
    #Remove missing values in predictors
    for pred in predictors:
       print('pred',pred)
       sel=trans[trans['name']==pred]
       print('sel',sel) 
       if not sel['type'].values==2 :
           print('drop', pred)
           print('evalset', evaldataset.shape)
           evaldataset.dropna(subset=pred,inplace=True)
           print('evalset', evaldataset.shape)
    if verbose:
       print('Use one hots', use_oh)
       print('ct',ct) 
       print('Dataset shape:',  evaldataset.shape)
    
    classifiers=generate_classifiers()
    d , bestd =evaluate_classifiers_transformer(evaldataset,feat_pred,predictors,ct, classes, classifiers, important=important_class, metric=metric)
    cache.set("models",d)
    #cache.set("ohd",ohd)
    cache.set("ohd",ct)
    for k,v in d.items():
         #print(np_html(v['cf']))
         v['cf_html']=np_html(v['cf'], classes_dict)
         #print(np_html(v['cf']))
    bestd['cf_html']=np_html(bestd['cf'], classes_dict)
    #Additional metrics:
    tp=bestd['cf'][1,1]
    tn=bestd['cf'][0,0]
    fn=bestd['cf'][0,1]
    fp=bestd['cf'][1,0]
    
    specificity=tn/(tn+fp)
    tpr=tp/(tp+fn)
    balanced_acc=(tpr+specificity)/2
    admet=[specificity, tpr, balanced_acc]
    #save model
    mname=next(tempfile._get_candidate_names())
    mpath=path.join(tmpdir,mname)
    dump(bestd['model'],mpath+'.pkl')
    dump(ct,mpath+'-tr.pkl')
    session['mpath']=mpath
    #Add more info: 
    report=dataset[feat_list].iloc[bestd['index'].values].copy()
    subs_ley(report, ley_d) 
    report.insert(0,'real', bestd['y_real'])
    report.insert(1,'pred', bestd['y_pred'])
    keys=list(classes_dict)
    important_name=classes_dict[keys[important_class]]
    if verbose:
       print('important_class:',important_class, 'important_name',important_name)
    for i in range(bestd['prob'].shape[1]):
       report.insert(2+i,'prob_'+classes_dict[keys[i]], bestd['prob'][:,i])
    fvd,co = make_feat_val_dict(  feat_list,ley_d)
    numbers=[]
    selects=[] 
    lists=[]
    for i in feat_list:
         print(i,ley_d[i])
         if ley_d[i]['type']==0:
            numbers.append({'name':i,'label':i})
         elif ley_d[i]['type']==1:
            selects.append({'name': i, 'options':ley_d[i]['legend'], 'label':i})
         else:
            lists.append({'name':i,'label':i})
    
    tname=ui_lang+'show_evaluation.html'
    return render_template (
        tname,
        d=d,
	bestd=bestd,
        feat_list=feat_list,
        fvd=fvd,
        co=co,
	feat_pred=feat_pred,
        tmpdir=tmpdir,
        filename=mname,
        mpath=mpath,
        classes=[v  for k,v in  classes_dict.items()],
        ratios=ratios,
        important_name=important_name,
        report=report.to_html(index=False,classes='table table-bordered'),
        numbers=numbers,
        selects=selects,
        lists=lists,
        admet=admet
	) 

@app.route('/get_prediction/', methods=['GET', 'POST'])
def get_prediction():
      feat_list=request.form.getlist("feat_list")
      mpath=request.form.get("mpath")
      d={}
      print('feat_list:',feat_list)
      print('feat_list:',eval(feat_list[0]))
      for i in eval(feat_list[0]):
         vr=request.form.get(i)
         try:
           v=float(vr)
           print('inum v',v)
         except ValueError as e:
           v=vr 
         #if vr.isnumeric():
         #     v=float(vr)
         #     print('inum v',v)
         #else:
         #     v=vr
         d[i]=v
         print('i',i)
         print('v',v)
      x=pd.DataFrame(d, index=[0])
      print('getting prediction:',feat_list, x.head())
      ct=load(mpath+'-tr.pkl') 
      X=ct.transform(x)
      model = load(mpath+'.pkl')
    
      value=model.predict(X)
      prob=model.predict_proba(X)     
      print('value',value,'prob',prob) 
      return render_template('pred.html', value=value, prob=prob)

@app.route('/download-model', methods=['GET', 'POST'])
def download_model():
    print(request.args)
    model=request.args.get("model")
    print(model)
    mname=next(tempfile._get_candidate_names())
    mpath=path.join(tmpdir,mname)
    d=cache.get("models")
    ohd=cache.get("ohd")
    print(d)
    dump(d[model],mpath+'.pkl')
    dump(ohd,mpath+'-tr.pkl')
    filenames = [mpath+".pkl", mpath+"-tr.pkl"]

    with zipfile.ZipFile(tmpdir+"/model.zip", mode="w") as archive:
       #for filename in filenames:
       archive.write(filenames[0], arcname=model+".pkl")
       archive.write(filenames[1], arcname=model+"-tr.pkl")
    return send_from_directory(tmpdir, "model.zip", as_attachment=True)
    
@app.route('/download/', methods=['GET', 'POST'])
def download_file():
    tmp=request.form.get("tmp")
    filename=request.form.get("filename")
    modelname=request.form.get("modelname")
    print('tmp',tmp)
    print('filename',filename)
    print('modelname',modelname)
    filenames = [tmp+"/"+filename+".pkl", tmp+"/"+filename+"-tr.pkl"]

    with zipfile.ZipFile(tmp+"/model.zip", mode="w") as archive:
       #for filename in filenames:
          archive.write(filenames[0], arcname=modelname+".pkl")
          archive.write(filenames[1], arcname=modelname+"-tr.pkl")
    return send_from_directory(tmp, "model.zip", as_attachment=True)

@app.route('/describe/')
def exec_describe():
    dataset=cache.get("dataset")
    if dataset is None:
         return redirect(location=url_for('select_dataset'))
    describe = dataset.describe()
    describe = describe.to_html(justify='center', table_id="myTable", border="0", classes="table-striped hover").replace('<th>', '<th style = "background-color: #007bff; color: white";>' )

    tname=ui_lang+'index.html'
    return render_template(
        tname,
        f_condition=False,
        describe=HTML(style + describe)
    )



@app.route('/hist_distribution/', methods=['GET', 'POST'])
def ejec_distribution_feature():
    dataset=cache.get("dataset")
    if dataset is None:
         return redirect(location=url_for('select_dataset'))
    atrib = request.form.get("distribution")
    try:
         hd=dataset[[atrib]].dropna()
         hist,edges = np.histogram(hd, density=False, bins=50)
         f=make_histogram_plot(atrib+" histogram", hist,edges)
         script_feature_distr, div_feature_distr = components(f)
         
         tname=ui_lang+'index.html'
         return render_template(
             tname,
             script_feature_distr=script_feature_distr,
             div_feature_distr=div_feature_distr,
             atrib=atrib,
             f_condition=False
         )
    except Exception as inst:
       
       tname=ui_lang+'error_hist.html'
       print(type(inst))    # the exception instance
       print(inst.args) 
       return render_template( tname, error='Feature cannot be represented as histogram', additional=inst.args)
        
@app.route('/distribution_condition/', methods=['POST', 'GET'])
def distribution_condition():
    dataset=cache.get("dataset")
    if dataset is None:
         return redirect(location=url_for('select_dataset'))
    atr = dataset.columns.sort_values().values.tolist()
    atributos=[]
    for i in atr:
        if not "List" in i:
           atributos.append(i)
    tname=ui_lang+'index.html'
    return render_template(
        tname,
        condition=True,
        atributos=atributos
    )




style = """
    <style>
        table#{random_id} {{text-align:center;}}
    </style>
    """.format(random_id='myTable')

style2 = """
    <style>
        table#{random_id} {{text-align:center;}}
    </style>
    """.format(random_id='myTable2')

style3 = """
    <style>
        table#{random_id} {{text-align:center;}}
    </style>
    """.format(random_id='myTable3')

style4 = """
    <style>
        table#{random_id} {{text-align:center;}}
    </style>
    """.format(random_id='myTable4')


if __name__ == '__main__':
    app.run(debug=True)
