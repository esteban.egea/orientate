# ORIENTATE
ORIENTATE, is a web application for automated application of machine learning classification algorithms by clinical practitioners lacking specific technical skills.
 ORIENTATE allows the  selection of features and the target variable, then automatically  generates a number of classification models and cross-validates them, finding the best model
and evaluating it. It also implements a custom feature selection algorithm for systematic searches of  the best combination of predictors for a given target variable.
Finally, it outputs an extensive report  with SHAP graphs that facilitates the explanation of the
classification model results, using global interpretation methods, and an interface for the prediction of new input samples.
Feature relevance and interaction plots provided by ORIENTATE allows to use it for statistical inference that can replace and/or complement classical statistical studies.

Further information will can be found in our related paper.

I. Gomez-Rios, E. Egea-Lopez, A. J. Ortiz Ruiz, "ORIENTATE: automated machine learning classifiers for oral health prediction and research" BMC Oral Health 23, 408, 2023 doi: [10.1186/s12903-023-03112-w](https://doi.org/10.1186/s12903-023-03112-w)

ORIENTATE is independent of the dataset used as long as it is provided in the format required. Datasets can be uploaded to the application and then selected for current use.

## Dataset format
Datasets are provided as CSV files.
Fore each dataset two CSV files are required: one with the dataset and another one with the dataset field information. The latter must have the following fields:
* name: name of the feature (one for each feature in the dataset) 
* type: an integer with the type of data. 0 for numeric or text columns, 1 for categorical columns, 2 for tooth-list (according to ISO 3950:2016 standard)  
* max-range: only for tooth-list features, indicate the maximum teeth number, typically 86. Leave empty for other columns  
* categories: only for categorical features, integer for each category, separated by semi-colon (;).  Leave empty for other columns 
* legend: only for categorical features, text for each category, separated by semi-colon (;).  Leave empty for other columns  

Two sample datasets that you can use are provided in the [sample_datasets](sample_datasets) folder. The [first one](sample_datasets/sedation2018.csv) was used in our currently reviewed paper and in [this paper](https://www.mdpi.com/1660-4601/20/4/3435). The other one has been retrieved from [zenodo](https://doi.org/10.5281/zenodo.1227714). Both can be used with ORIENTATE since we have added the dataset field information. Note that the first dataset uses `;` as field separator while the second one uses  `,`. Select the correct separator in the application when uploading the dataset.

## Instalation
### Requirements
python v3.9.12, flask v2.0.3, scikit-learn v1.1.1, shap v0.41.0, flask-cache v0.13.0.1, bokeh v2.4.3, xgboost v1.6.1, graphviz v0.20.

### Steps to install
1. Install the required libraries with pip or conda
2. Clone or download the repository
3. Fill the `engine` variable in [database.py](flask-app/database.py)
4. Run the [createdb.py](flask-app/createdb.py) usually with `python createdb.py` to create the database file
5. Run the flask application, usually just by going to the `flask-app` folder and run `flask run`. You can find additional instructions in the Flask website.
6. Open your browser and type `localhost:5000`.
 
